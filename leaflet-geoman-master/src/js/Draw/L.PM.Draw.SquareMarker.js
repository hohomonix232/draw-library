import Draw from './L.PM.Draw';
import { getTranslation } from '../helpers';

Draw.SquareMarker = Draw.Marker.extend({
  initialize: function initialize(map) {
    this._map = map;
    this._shape = 'SquareMarker';
    this.toolbarButtonName = 'drawSquareMarker';
  },
  enable: function enable(options) {
    var _this = this;

    // TODO: Think about if these options could be passed globally for all
    // instances of L.PM.Draw. So a dev could set drawing style one time as some kind of config
    L.Util.setOptions(this, options); // change enabled state

    this._enabled = true; // create a marker on click on the map

    this._map.on('click', this._createMarker, this); // toggle the draw button of the Toolbar in case drawing mode got enabled without the button


    this._map.pm.Toolbar.toggleButton(this.toolbarButtonName, true); // this is the hintmarker on the mouse cursor


    this._hintMarker = L.shapeSquareMarker([0, 0], this.options.squareMarker);
    this._hintMarker._pmTempLayer = true;

    this._hintMarker.addTo(this._map); // add tooltip to hintmarker


    if (this.options.tooltips) {
      this._hintMarker.bindTooltip(getTranslation('tooltips.placeMarker'), {
        permanent: true,
        offset: L.point(0, 10),
        direction: 'bottom',
        opacity: 0.8
      }).openTooltip();
    } // this is just to keep the snappable mixin happy


    this._layer = this._hintMarker; // sync hint marker with mouse cursor

    this._map.on('mousemove', this._syncHintMarker, this); // fire drawstart event


    this._map.fire('pm:drawstart', {
      shape: this._shape,
      workingLayer: this._layer
    }); // enable edit mode for existing markers


    this._map.eachLayer(function (layer) {
      if (_this.isRelevantMarker(layer)) {
        layer.pm.enable();
      }
    });
  },
  isRelevantMarker: function isRelevantMarker(layer) {
    return layer instanceof L.shapeSquareMarker && !(layer instanceof L.Circle) && layer.pm && !layer._pmTempLayer;
  },
  _createMarker: function _createMarker(e) {
    if (!e.latlng) {
      return;
    } // assign the coordinate of the click to the hintMarker, that's necessary for
    // mobile where the marker can't follow a cursor


    if (!this._hintMarker._snapped) {
      this._hintMarker.setLatLng(e.latlng);
    } // get coordinate for new vertex by hintMarker (cursor marker)


    var latlng = this._hintMarker.getLatLng(); // create marker


    var marker = L.shapeSquareMarker(latlng, this.options.squareMarker);

    marker.addTo(this._map); // enable editing for the marker

    //marker.pm.enable(); // fire the pm:create event and pass shape and marker

    this._map.fire('pm:create', {
      shape: this._shape,
      marker: marker,
      // DEPRECATED
      layer: marker
    });

    this._cleanupSnapping();
  }
});
L.SVG.include({
  _updateShape: function _updateShape(layer) {
    var p = layer._point;
    var s = layer._radius;
    var shape = layer.options.shape;

    if(shape === "square"){
        var d = "M"+ (p.x-s)+ " "+ (p.y-s)+ " L " + (p.x+s) +" "+ (p.y-s)+ " L"  + (p.x+s) + " " + (p.y+s)+ " L"  + (p.x-s) + " " + (p.y+s) +" L"  + (p.x-s) + " " + (p.y-s);
        this._setPath(layer, d);
    }
    
    if (shape === "triangle") {
        var d = "M" + (p.x - (13/10*s)) + " " + (p.y + (0.75*s)) + " L" + (p.x) + " " + (p.y - (1.5*s)) + " L" + (p.x + (13/10*s)) + " " + (p.y + (0.75*s)) + " Z";
        this._setPath(layer, d);
    }
  }
});
L.ShapeSquareMarker = L.Path.extend({
  options: {
    fill: true,
    shape: 'square',
    radius: 10
  },
  initialize: function initialize(latlng, options) {
    L.setOptions(this, options);
    this._latlng = L.latLng(latlng);
    this._radius = this.options.radius;
  },
  setLatLng: function setLatLng(latlng) {
    this._latlng = L.latLng(latlng);
    this.redraw();
    return this.fire('move', {
      latlng: this._latlng
    });
  },
  getLatLng: function getLatLng() {
    return this._latlng;
  },
  setRadius: function setRadius(radius) {
    this.options.radius = this._radius = radius;
    return this.redraw();
  },
  getRadius: function getRadius() {
    return this._radius;
  },
  setStyle: function setStyle(options) {
    var radius = options && options.radius || this._radius;
    L.Path.prototype.setStyle.call(this, options);
    this.setRadius(radius);
    return this;
  },
  _project: function _project() {
    this._point = this._map.latLngToLayerPoint(this._latlng);

    this._updateBounds();
  },
  _updateBounds: function _updateBounds() {
    var r = this._radius,
        r2 = this._radiusY || r,
        w = this._clickTolerance(),
        p = [r + w, r2 + w];

    this._pxBounds = new L.Bounds(this._point.subtract(p), this._point.add(p));
  },
  _update: function _update() {
    if (this._map) {
      this._updatePath();
    }
  },
  _updatePath: function _updatePath() {
    this._renderer._updateShape(this);
  },
  _empty: function _empty() {
    return this._size && !this._renderer._bounds.intersects(this._pxBounds);
  },
  toGeoJSON: function toGeoJSON() {
    return L.GeoJSON.getFeature(this, {
      type: 'Point',
      coordinates: L.GeoJSON.latLngToCoords(this.getLatLng())
    });
  }
}); 

L.shapeSquareMarker = function shapeMarker(latlng, options) {
  return new L.ShapeSquareMarker(latlng, options);
};